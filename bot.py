import asyncio
import datetime
from uuid import uuid4
import logging
from os import getenv
from aiogram.types.inline_query import InlineQuery
from aiogram.types.inline_query_result import InlineQueryResultCachedSticker
from concurrent.futures import ThreadPoolExecutor
from io import BytesIO

from aiogram import Bot, Dispatcher

import logwood, logwood.compat
from logwood.handlers.stderr import ColoredStderrHandler

from src.compositions.local import *
from src.compositions.foreign import *
from src.compositions.awoo import *

last_worker_ix = 0
worker_bots = []


async def inline_query_handler(inline_query: InlineQuery):
	watching_datetime = datetime.datetime.now()
	global last_worker_ix
	global worker_bots
	channel_id_caching = int(getenv('CACHING_CHANNEL_ID'))
	channel_id_logging = int(getenv('LOGGING_CHANNEL_ID'))
	query_text = inline_query.query
	username = inline_query.from_user.username
	logging.info(f"New query from '{username}' - '{query_text}'")
	sticker_storage = [
		# lambda: baka_freem.get_bytes(query_text),
		lambda: baka_freem_roboto.get_bytes(query_text),
		# lambda: baka_oggy.get_bytes(query_text),
		# lambda: baka_azshara.get_bytes(query_text),
		# lambda: baka_dbleki.get_bytes(query_text),
		# lambda: sign_radar.get_bytes(query_text),
		# lambda: baka_daxter.get_bytes(query_text),
		lambda: sign_dusk.get_bytes(query_text),
		# lambda: sign_aito.get_bytes(query_text),
		# lambda: exclaim_akira.get_bytes(query_text),
		# lambda: kestral_angry.get_bytes(query_text),
		# lambda: lynx_catch.get_bytes(query_text),
		# lambda: ryker_cape.get_bytes(query_text),
		# lambda: vulan_crying.get_bytes(query_text),
		# lambda: jfet_angry.get_bytes(query_text),
		# lambda: snep_show.get_bytes(query_text),
		# lambda: farfalle_belly.get_bytes(query_text),
		# lambda: awoo_morning.get_bytes(query_text),
		# lambda: awoo_angry.get_bytes(query_text),
		# lambda: awoo_scream.get_bytes(query_text),
	]
	sticker_bytes = []
	with ThreadPoolExecutor() as executor:
		running_tasks = [executor.submit(sticker_lambda) for sticker_lambda in sticker_storage]
		for index, task in enumerate(running_tasks):
			res = task.result()
			if res is not None:
				bytes_io_obj = BytesIO(bytes(res))
				bytes_io_obj.name = f'sticker_{index}.webp'
				sticker_bytes.append(bytes_io_obj)
	logging.info(f'Stickers composed')
	sticker_ids = []
	for sticker in sticker_bytes:
		sticker_ids.append(
			(
				await worker_bots[last_worker_ix].send_document(chat_id = channel_id_caching, document = sticker)
			).sticker.file_id
		)
		last_worker_ix = (last_worker_ix + 1) % len(worker_bots)
	logging.info(f'Stickers sent')
	inline_query_responses = [
		InlineQueryResultCachedSticker(sticker_file_id = file_id, id = uuid4().hex)
		for file_id in sticker_ids
	]
	await inline_query.answer(results = inline_query_responses)
	logging.info('Inline query answered')
	took = (datetime.datetime.now() - watching_datetime).microseconds / 1000000.0
	await inline_query.bot.send_message(
		chat_id = channel_id_logging,
		text = f'**Composed query**\n  **From**: @{username}\n  **Took**: `{took:.1f} seconds`\n  **Query**: `{query_text}`',
		parse_mode = 'MarkdownV2',
	)
	logging.info(f'Composing {len(sticker_bytes)} stickers took {took:.1f} seconds')


async def main():
	logwood.basic_config(level = logwood.INFO, handlers = [ColoredStderrHandler()])
	logwood.compat.redirect_standard_logging()

	logging.info('Getting env vars')
	bot_token = getenv('TELEGRAM_API_KEY')

	logging.info('Starting main bot')
	bot = Bot(token = bot_token, connections_limit = 32)
	global worker_bots
	worker_api_keys = getenv('WORKER_API_KEYS').split(';')
	logging.info(f'Starting {len(worker_api_keys)} worker bots')
	worker_bots = [Bot(token = bot_token, connections_limit = 2) for bot_token in worker_api_keys]
	try:
		logging.info('Getting dispatcher')
		dispatcher = Dispatcher(bot = bot)
		logging.info('Registering message handler')
		dispatcher.register_inline_handler(inline_query_handler)
		logging.info('Starting polling')
		await dispatcher.start_polling()
	finally:
		logging.info('Closing bot connections')
		await bot.close()
		for worker_bot in worker_bots:
			await worker_bot.close()


if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	loop.run_until_complete(main())
