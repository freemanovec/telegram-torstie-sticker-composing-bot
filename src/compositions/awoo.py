from src.compositions.common import Composition

awoo_morning = Composition(
	source_filename = 'sources/awoo_comic_morning.png',
	canvas_control_points = [[57.0, 19.0], [414.0, 18.0], [417.0, 163.0], [57.0, 170.0]],
	canvas_padding = [10, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/ComicSans-Regular.ttf',
	font_color = [0, 0, 0, 255],
)

awoo_angry = Composition(
	source_filename = 'sources/awoo_comic_angry.png',
	canvas_control_points = [[6.0, 0.0], [512.0, 0.0], [512.0, 124.0], [6.0, 124.0]],
	canvas_padding = [0, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/ComicSans-Regular.ttf',
	font_color = [0, 0, 0, 255],
)

awoo_scream = Composition(
	source_filename = 'sources/awoo_comic_scream.png',
	canvas_control_points = [[0.0, 0.0], [457.0, 0.0], [457.0, 412.0], [0.0, 412.0]],
	canvas_padding = [0, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/ComicSans-Regular.ttf',
	font_color = [0, 0, 0, 255],
)
