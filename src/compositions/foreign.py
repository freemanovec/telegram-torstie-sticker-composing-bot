from src.compositions.common import Composition

sign_aito = Composition(
	source_filename = 'sources/aito_sign.png',
	canvas_control_points = [[33.0, 290.0], [476.0, 291.0], [476.0, 482.0], [31.0, 482.0]],
	canvas_padding = [3, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

exclaim_akira = Composition(
	source_filename = 'sources/akira_exclaim.png',
	canvas_control_points = [[40.0, 82.0], [252.0, 13.0], [292.0, 125.0], [93.0, 206.0]],
	canvas_padding = [10, 4],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

kestral_angry = Composition(
	source_filename = 'sources/kestral_angry.png',
	canvas_control_points = [[78.0, 347.0], [450.0, 291.0], [476.0, 437.0], [102.0, 490.0]],
	canvas_padding = [10, 8],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

lynx_catch = Composition(
	source_filename = 'sources/lynx_catch.png',
	canvas_control_points = [[44.0, 30.0], [444.0, 30.0], [444.0, 86.0], [45.0, 86.0]],
	canvas_padding = [5, 3],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

ryker_cape = Composition(
	source_filename = 'sources/ryker_cape.png',
	canvas_control_points = [[51.0, 303.0], [165.0, 279.0], [183.0, 364.0], [64.0, 389.0]],
	canvas_padding = [3, 5],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

vulan_crying = Composition(
	source_filename = 'sources/vulan_crying.png',
	canvas_control_points = [[47.0, 16.0], [410.0, 44.0], [395.0, 224.0], [26.0, 203.0]],
	canvas_padding = [3, 4],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Fruktur-Regular.ttf',
	font_color = [0, 0, 0, 255],
)

jfet_angry = Composition(
	source_filename = 'sources/jfet_angry.png',
	canvas_control_points = [[3.25, 230.17], [338.25, 218.17], [341.75, 505.0], [10.25, 530.0]],
	canvas_padding = [10, 16],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [200, 0, 0, 255],
)

snep_show = Composition(
	source_filename = 'sources/snep_show.png',
	canvas_control_points = [[28.0, 84.0], [204.0, 30.0], [231.0, 130.0], [68.0, 178.0]],
	canvas_padding = [5, 4],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

farfalle_belly = Composition(
	source_filename = 'sources/farfalle_belly.png',
	canvas_control_points = [[82.0, 59.0], [366.0, 42.0], [388.0, 159.0], [104.0, 182.0]],
	canvas_padding = [5, 4],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)
