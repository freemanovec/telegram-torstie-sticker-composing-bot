from math import sqrt, ceil
import sticker_composer


class Composition:
	def __init__(
		self,
		source_filename,
		canvas_control_points,
		canvas_padding = (10, 5),
		canvas_color = (255, 255, 255, 255),
		font_filename = 'fonts/Kalam-Bold.ttf',
		font_color = (0, 0, 0, 255),
	):
		with open(source_filename, 'rb') as f:
			self.source_image = f.read()
		self.canvas_size = self._calculate_canvas_size(canvas_control_points)
		self.canvas_control_points = canvas_control_points
		self.canvas_padding = canvas_padding
		self.canvas_color = canvas_color
		with open(font_filename, 'rb') as f:
			self.font_bytes = f.read()
		self.font_color = font_color

	def get_bytes(self, query):
		return self._get_composed_image(query, None)

	def _get_composed_image(self, query, cancellation_token):

		composed_bytes = sticker_composer.compose_text(
			self.source_image,
			self.canvas_control_points[0][0],
			self.canvas_control_points[0][1],
			self.canvas_control_points[1][0],
			self.canvas_control_points[1][1],
			self.canvas_control_points[2][0],
			self.canvas_control_points[2][1],
			self.canvas_control_points[3][0],
			self.canvas_control_points[3][1],
			self.canvas_padding[0],
			self.canvas_padding[1],
			self.canvas_color[0],
			self.canvas_color[1],
			self.canvas_color[2],
			self.canvas_color[3],
			self.font_bytes,
			self.font_color[0],
			self.font_color[1],
			self.font_color[2],
			self.font_color[3],
			query,
		)

		if len(composed_bytes) == 0:
			return None

		return composed_bytes

	def _calculate_canvas_size(self, points):
		def point_distance(point_a, point_b):
			return sqrt((abs((point_a[0] - point_b[0])) ** 2) + (abs((point_a[1] - point_b[1])) ** 2))

		left = point_distance(points[0], points[3])
		right = point_distance(points[1], points[2])
		height = left if left > right else right
		top = point_distance(points[0], points[1])
		bottom = point_distance(points[2], points[3])
		width = top if top > bottom else bottom
		return [ceil(width), ceil(height)]
