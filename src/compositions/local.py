from src.compositions.common import Composition

baka_freem = Composition(
	source_filename = 'sources/freem_baka.png',
	canvas_control_points = [[72.0, 189.0], [385.0, 246.0], [338.0, 487.0], [23.0, 425.0]],
	canvas_padding = [15, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

baka_freem_roboto = Composition(
	source_filename = 'sources/freem_baka.png',
	canvas_control_points = [[72.0, 189.0], [385.0, 246.0], [338.0, 487.0], [23.0, 425.0]],
	canvas_padding = [15, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

baka_oggy = Composition(
	source_filename = 'sources/oggy_baka.png',
	canvas_control_points = [[81.0, 176.0], [401.0, 236.0], [353.0, 483.0], [30.0, 420.0]],
	canvas_padding = [15, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

baka_azshara = Composition(
	source_filename = 'sources/azshara_baka.png',
	canvas_control_points = [[71.0, 172.0], [404.0, 235.0], [354.0, 491.0], [19.0, 425.0]],
	canvas_padding = [15, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

baka_dbleki = Composition(
	source_filename = 'sources/dbleki_baka.png',
	canvas_control_points = [[71.0, 172.0], [404.0, 235.0], [354.0, 491.0], [19.0, 425.0]],
	canvas_padding = [15, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

sign_radar = Composition(
	source_filename = 'sources/radar_sign.png',
	canvas_control_points = [[79.0, 328.0], [343.0, 327.0], [343.0, 501.0], [88.0, 492.0]],
	canvas_padding = [5, 15],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)

baka_daxter = Composition(
	source_filename = 'sources/daxter_baka.png',
	canvas_control_points = [[79.0, 178.0], [383.0, 235.0], [340.0, 472.0], [34.0, 411.0]],
	canvas_padding = [15, 7],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Roboto-Black.ttf',
	font_color = [0, 0, 0, 255],
)

sign_dusk = Composition(
	source_filename = 'sources/dusk_sign.png',
	canvas_control_points = [[108.0, 307.0], [308.0, 305.0], [291.0, 502.0], [76.0, 491.0]],
	canvas_padding = [20, 10],
	canvas_color = [255, 255, 255, 255],
	font_filename = 'fonts/Kalam-Bold.ttf',
	font_color = [0, 0, 0, 255],
)
