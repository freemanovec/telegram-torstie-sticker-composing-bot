pub mod composing;

extern crate cpython;

use composing::{composition::Composition, utils::ImageManipulator};
use cpython::{
    PyResult,
    Python,
    py_module_initializer,
    py_fn
};
use image::EncodableLayout;

py_module_initializer!(sticker_composer, |py, module| {
    module.add(py, "__doc__", "Module for composing stickers")?;
    module.add(py, "compose_text", py_fn!(py, compose_text(
        source_png_bytes: Vec<u8>,
        canvas_control_point_a_x: f32,
        canvas_control_point_a_y: f32,
        canvas_control_point_b_x: f32,
        canvas_control_point_b_y: f32,
        canvas_control_point_c_x: f32,
        canvas_control_point_c_y: f32,
        canvas_control_point_d_x: f32,
        canvas_control_point_d_y: f32,
        canvas_padding_width: u32,
        canvas_padding_height: u32,
        canvas_color_red: u8,
        canvas_color_green: u8,
        canvas_color_blue: u8,
        canvas_color_alpha: u8,
        font_file_bytes: Vec<u8>,
        font_color_red: u8,
        font_color_green: u8,
        font_color_blue: u8,
        font_color_alpha: u8,
        text: &str
    )))?;
    module.add(py, "compose_image", py_fn!(py, compose_image(
        source_png_bytes: Vec<u8>,
        canvas_control_point_a_x: f32,
        canvas_control_point_a_y: f32,
        canvas_control_point_b_x: f32,
        canvas_control_point_b_y: f32,
        canvas_control_point_c_x: f32,
        canvas_control_point_c_y: f32,
        canvas_control_point_d_x: f32,
        canvas_control_point_d_y: f32,
        canvas_padding_width: u32,
        canvas_padding_height: u32,
        canvas_color_red: u8,
        canvas_color_green: u8,
        canvas_color_blue: u8,
        canvas_color_alpha: u8,
        font_file_bytes: Vec<u8>,
        font_color_red: u8,
        font_color_green: u8,
        font_color_blue: u8,
        font_color_alpha: u8,
        image: Vec<u8>
    )))?;
    Ok(())
});

fn compose_text(
    _py: Python,
    source_png_bytes: Vec<u8>,
    canvas_control_point_a_x: f32,
    canvas_control_point_a_y: f32,
    canvas_control_point_b_x: f32,
    canvas_control_point_b_y: f32,
    canvas_control_point_c_x: f32,
    canvas_control_point_c_y: f32,
    canvas_control_point_d_x: f32,
    canvas_control_point_d_y: f32,
    canvas_padding_width: u32,
    canvas_padding_height: u32,
    canvas_color_red: u8,
    canvas_color_green: u8,
    canvas_color_blue: u8,
    canvas_color_alpha: u8,
    font_file_bytes: Vec<u8>,
    font_color_red: u8,
    font_color_green: u8,
    font_color_blue: u8,
    font_color_alpha: u8,
    text: &str
) -> PyResult<Vec<u8>> {
    let composition = Composition::new(
        source_png_bytes,
        [
            (canvas_control_point_a_x, canvas_control_point_a_y),
            (canvas_control_point_b_x, canvas_control_point_b_y),
            (canvas_control_point_c_x, canvas_control_point_c_y),
            (canvas_control_point_d_x, canvas_control_point_d_y)
        ],
        (canvas_padding_width, canvas_padding_height),
        [
            canvas_color_red,
            canvas_color_green,
            canvas_color_blue,
            canvas_color_alpha
        ],
        font_file_bytes,
        [
            font_color_red,
            font_color_green,
            font_color_blue,
            font_color_alpha
        ]
    );
    let image = composition.compose_text(String::from(text));
    let webp = ImageManipulator::encode_webp(image.as_bytes(), image.dimensions().0, image.dimensions().1, 100);
    if let Some(webp) = webp {
        Ok(webp)
    } else {
        Ok(vec![])
    }
}

fn compose_image(
    _py: Python,
    source_png_bytes: Vec<u8>,
    canvas_control_point_a_x: f32,
    canvas_control_point_a_y: f32,
    canvas_control_point_b_x: f32,
    canvas_control_point_b_y: f32,
    canvas_control_point_c_x: f32,
    canvas_control_point_c_y: f32,
    canvas_control_point_d_x: f32,
    canvas_control_point_d_y: f32,
    canvas_padding_width: u32,
    canvas_padding_height: u32,
    canvas_color_red: u8,
    canvas_color_green: u8,
    canvas_color_blue: u8,
    canvas_color_alpha: u8,
    font_file_bytes: Vec<u8>,
    font_color_red: u8,
    font_color_green: u8,
    font_color_blue: u8,
    font_color_alpha: u8,
    image: Vec<u8>
) -> PyResult<Vec<u8>> {
    let composition = Composition::new(
        source_png_bytes,
        [
            (canvas_control_point_a_x, canvas_control_point_a_y),
            (canvas_control_point_b_x, canvas_control_point_b_y),
            (canvas_control_point_c_x, canvas_control_point_c_y),
            (canvas_control_point_d_x, canvas_control_point_d_y)
        ],
        (canvas_padding_width, canvas_padding_height),
        [
            canvas_color_red,
            canvas_color_green,
            canvas_color_blue,
            canvas_color_alpha
        ],
        font_file_bytes,
        [
            font_color_red,
            font_color_green,
            font_color_blue,
            font_color_alpha
        ]
    );
    let image = image::load_from_memory(&image[..]);
    if let Ok(image) = image {
        let image = composition.compose_image(image.into_rgba());
        let webp = ImageManipulator::encode_webp(image.as_bytes(), image.dimensions().0, image.dimensions().1, 100);
        if let Some(webp) = webp {
            return Ok(webp);
        }
    }
    Ok(vec![])
}
