FROM rust:buster as composition_build

WORKDIR /project
COPY composition_lib composition_lib/
RUN (cd composition_lib && cargo build --release)

FROM python:3.9-buster

COPY requirements.txt ./
RUN python3 -m pip install -r requirements.txt
USER nobody
WORKDIR /project
COPY --chown=nobody:nogroup . .
COPY --from=composition_build --chown=nobody:nogroup /project/composition_lib/target/release/libsticker_composer.so ./sticker_composer.so
ENTRYPOINT [ "python3", "-u", "bot.py" ]
